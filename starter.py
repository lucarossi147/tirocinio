import requests
import subprocess

import time
from time import sleep

start=time.time()
connecting = True
cicles = 0

def startProgram(command, filepath):
    global connecting
    connecting = False
    subprocess.call([command, filepath, "&"])

while connecting:
    try:
        #se c'è connessione uso il server altrimenti vado con la versione embedded anche se puzza
        r = requests.get('http://google.com', timeout=0.2)
        if r.status_code == 200:
            startProgram("/home/pi/tirocinio/eye/bin/python","/home/pi/tirocinio/eye.py")
    except:
        print("echo" ,"waiting for connection")
        sleep(1)
        cicles += 1
        if cicles > 1 and connecting is True:
            #startProgram("/home/pi/sad/bin/python3.7","/home/pi/tirocinio/vision.py")
            subprocess.call(["sudo","./vision.sh"])
