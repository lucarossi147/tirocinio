import subprocess
import json


def play(class_instance):
    path_to_audio_file = 'voices/'+class_instance+'.mp3'
    subprocess.call(["mpg321", path_to_audio_file])

with open('classes.json') as f:
  classes_dict = json.load(f)

classes = classes_dict['classes']

for class_instance in classes:
    play(class_instance)