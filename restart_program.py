import subprocess
from time import sleep
import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
import threading

wait_mode = True

#directly calls the starter.py
def restart_program():
    subprocess.call(['/home/pi/tirocinio/eye/bin/python', '/home/pi/tirocinio/starter.py', '&'])

def set_wait_mode_OFF(channel):
    if GPIO.input(channel) == GPIO.HIGH:
        global wait_mode
        wait_mode = False


#button to start program
GPIO.setup(29, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.add_event_detect(29, GPIO.BOTH, callback=set_wait_mode_OFF)
try:
    while wait_mode:
        sleep(0.5)
    t = threading.Thread(target=restart_program)
    t.start()
except:
    print("program stopped from keyboard")

    
