import RPi.GPIO as GPIO
import time
from picamera import PiCamera
from time import sleep
import requests

import subprocess
import json
import threading

###############
############
#camera
##########################
camera = PiCamera()
camera.rotation = 180
camera.resolution = (640,192)
camera.start_preview(alpha=150)

start = time.time()

#defining led pins
LedPin1 = 37
LedPin2 = 36
LedPin3 = 38
LedPin4 = 40
LedPin5 = 32
LedPin6 = 13
LedPin7 = 11
LedPin8 = 19

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

GPIO.setup(LedPin1, GPIO.OUT)
GPIO.setup(LedPin2, GPIO.OUT)
GPIO.setup(LedPin3, GPIO.OUT)
GPIO.setup(LedPin4, GPIO.OUT)
GPIO.setup(LedPin5, GPIO.OUT)
GPIO.setup(LedPin6, GPIO.OUT)
GPIO.setup(LedPin7, GPIO.OUT)
GPIO.setup(LedPin8, GPIO.OUT)

GPIO.output(LedPin1, GPIO.LOW)
GPIO.output(LedPin2, GPIO.LOW)
GPIO.output(LedPin3, GPIO.LOW)
GPIO.output(LedPin4, GPIO.LOW)
GPIO.output(LedPin5, GPIO.LOW)
GPIO.output(LedPin6, GPIO.LOW)
GPIO.output(LedPin7, GPIO.LOW)
GPIO.output(LedPin8, GPIO.LOW)

p1 = GPIO.PWM(LedPin1,50)
p2 = GPIO.PWM(LedPin2,50)
p3 = GPIO.PWM(LedPin3,50)
p4 = GPIO.PWM(LedPin4,50)
p5 = GPIO.PWM(LedPin5,50)
p6 = GPIO.PWM(LedPin6,50)
p7 = GPIO.PWM(LedPin7,50)
p8 = GPIO.PWM(LedPin8,50)

pins = list()
pins.append(p1)
pins.append(p2)
pins.append(p3)
pins.append(p4)
pins.append(p5)
pins.append(p6)
pins.append(p7)
pins.append(p8)

def apply_values_to_leds(pins,values):
    pins[0].start(values[0])
    pins[1].start(values[1])
    pins[2].start(values[2])
    pins[3].start(values[3])
    pins[4].start(values[4])
    pins[5].start(values[5])
    pins[6].start(values[6])
    pins[7].start(values[7])

def set_workingFlag_OFF(channel):
    if GPIO.input(channel) == GPIO.HIGH:
        global workingFlag
        workingFlag = False

def set_describeFlag_ON(channel):
    if GPIO.input(channel) == GPIO.HIGH:
        global describeFlag
        describeFlag = True

#the output is strangely on stderr and not on stdout
def play_voices_in_background():
    subprocess.call(['/home/pi/tirocinio/eye/bin/python', 'reader.py'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def restart_program():
    subprocess.call(['/home/pi/tirocinio/eye/bin/python', 'restart_program.py'])

#button to stop program
GPIO.setup(29, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.add_event_detect(29, GPIO.BOTH, callback=set_workingFlag_OFF)

#button to describe ambient
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.add_event_detect(16, GPIO.BOTH, callback=set_describeFlag_ON)

#defining list of values
values = [0]*8
#defining url paths
img_url = 'http://192.168.0.14:5000/img'
image_path = "/home/pi/tirocinio/assets/eye.jpg"
#defining flags
workingFlag = True
describeFlag = False

#measure time, if > 2 seconds don't need to wait.
#it takes more or less 0.2 seconds
end = time.time()
print (end - start)

if (end - start) < 2:
    print("waiting for light sensor")
    sleep(2 - (end - start))

while workingFlag:
    start = time.time()
    
    #take photo
    camera.capture(image_path)
    
    #send image
    img_to_analyze = {'image':open(image_path, 'rb')}
    r = requests.post(img_url, files = img_to_analyze)
    
    #recieve response
    if r.status_code == 200:
        r = requests.get('http://192.168.0.14:5000/')
        values, classes = r.json()
        end = time.time()
        print(values)
        print(classes)

        #reads out loud the classes
        if describeFlag:
            classes_dict = {"classes" : classes}
            with open('classes.json', 'w') as f:
                json.dump(classes_dict, f)
            t = threading.Thread(target=play_voices_in_background)
            t.daemon = True
            t.start()
            describeFlag = False
        apply_values_to_leds(pins, values)
        print('-> Done!')
        print(end - start)
    
    else:
        print('network error')
    
camera.stop_preview()
camera.close()
p1.stop()
p2.stop()
p3.stop()
p4.stop()
p5.stop()
p6.stop()
p7.stop()
p8.stop()
GPIO.cleanup()

#calls another thread hearing for the restart of the program
t = threading.Thread(target=restart_program)
t.start()