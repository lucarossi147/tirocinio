from __future__ import absolute_import, division, print_function

import os
import sys
import glob
import argparse
import numpy as np
import PIL.Image as pil
import matplotlib as mpl
import matplotlib.cm as cm

import torch
from torchvision import transforms, datasets

import networks
from layers import disp_to_depth
from utils import download_model_if_doesnt_exist

import RPi.GPIO as GPIO
import time
from picamera import PiCamera
from time import sleep

import cv2 as cv
import threading
import subprocess

##########################
#camera
##########################
camera = PiCamera()
camera.rotation = 180
camera.resolution = (640,192)
camera.start_preview(alpha=150)

start = time.time()

workingFlag = True


#defining led pins
LedPin1 = 37
LedPin2 = 36
LedPin3 = 38
LedPin4 = 40
LedPin5 = 32
LedPin6 = 13
LedPin7 = 11
LedPin8 = 19

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

GPIO.setup(LedPin1, GPIO.OUT)
GPIO.setup(LedPin2, GPIO.OUT)
GPIO.setup(LedPin3, GPIO.OUT)
GPIO.setup(LedPin4, GPIO.OUT)
GPIO.setup(LedPin5, GPIO.OUT)
GPIO.setup(LedPin6, GPIO.OUT)
GPIO.setup(LedPin7, GPIO.OUT)
GPIO.setup(LedPin8, GPIO.OUT)

GPIO.output(LedPin1, GPIO.LOW)
GPIO.output(LedPin2, GPIO.LOW)
GPIO.output(LedPin3, GPIO.LOW)
GPIO.output(LedPin4, GPIO.LOW)
GPIO.output(LedPin5, GPIO.LOW)
GPIO.output(LedPin6, GPIO.LOW)
GPIO.output(LedPin7, GPIO.LOW)
GPIO.output(LedPin8, GPIO.LOW)

p1 = GPIO.PWM(LedPin1,50)
p2 = GPIO.PWM(LedPin2,50)
p3 = GPIO.PWM(LedPin3,50)
p4 = GPIO.PWM(LedPin4,50)
p5 = GPIO.PWM(LedPin5,50)
p6 = GPIO.PWM(LedPin6,50)
p7 = GPIO.PWM(LedPin7,50)
p8 = GPIO.PWM(LedPin8,50)

pins = list()
pins.append(p1)
pins.append(p2)
pins.append(p3)
pins.append(p4)
pins.append(p5)
pins.append(p6)
pins.append(p7)
pins.append(p8)

def apply_values_to_leds(pins,values):
    pins[0].start(values[0])
    pins[1].start(values[1])
    pins[2].start(values[2])
    pins[3].start(values[3])
    pins[4].start(values[4])
    pins[5].start(values[5])
    pins[6].start(values[6])
    pins[7].start(values[7])

def my_callback(channel):
    if GPIO.input(channel) == GPIO.HIGH:
        global workingFlag
        workingFlag = False

def restart_program():
    subprocess.call(['/home/pi/tirocinio/eye/bin/python', 'restart_program.py'])

#button to stop program
GPIO.setup(29, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.add_event_detect(29, GPIO.BOTH, callback=my_callback)

#defining position for every led,
leds = list()
leds.append((0,0))
leds.append((0,158))
leds.append((0,316))
leds.append((0,637))
leds.append((180,0))
leds.append((189,158))
leds.append((189,316))
leds.append((189,637))
values = [0]*8

if torch.cuda.is_available() and not args.no_cuda:
        device = torch.device("cuda")
else:
    device = torch.device("cpu")

# download model if not present
download_model_if_doesnt_exist("mono_640x192")
model_path = os.path.join("models", "mono_640x192")
print("-> Loading model from ", model_path)
encoder_path = os.path.join(model_path, "encoder.pth")
depth_decoder_path = os.path.join(model_path, "depth.pth")

# LOADING PRETRAINED MODEL
print("   Loading pretrained encoder")
encoder = networks.ResnetEncoder(18, False)
loaded_dict_enc = torch.load(encoder_path, map_location=device)

# extract the height and width of image that this model was trained with
feed_height = loaded_dict_enc['height']
feed_width = loaded_dict_enc['width']
filtered_dict_enc = {k: v for k, v in loaded_dict_enc.items() if k in encoder.state_dict()}
encoder.load_state_dict(filtered_dict_enc)
encoder.to(device)
encoder.eval()

print("   Loading pretrained decoder")
depth_decoder = networks.DepthDecoder(
    num_ch_enc=encoder.num_ch_enc, scales=range(4))

loaded_dict = torch.load(depth_decoder_path, map_location=device)
depth_decoder.load_state_dict(loaded_dict)

depth_decoder.to(device)
depth_decoder.eval()

image_path = "/home/pi/tirocinio/assets/eye.jpg"
#measure time, if > 2 seconds don't need to wait.
#it takes more or less 1.7-1.8 seconds
end = time.time()
print (end - start)
if (end - start) < 2:
    print("waiting for sensor")
    sleep(2 - (end - start))

while workingFlag:
    #take photo
    camera.capture(image_path)
    
    # FINDING INPUT IMAGE
    if os.path.isfile(image_path):
        # Only testing on a single image
        paths = [image_path]
        output_directory = os.path.dirname(image_path)
    else:
        raise Exception("Can not find image_path: {}".format(image_path))

    print("-> Predicting on {:d} test image".format(len(paths)))

    # PREDICTING ON EACH IMAGE IN TURN
    with torch.no_grad():
        for idx, image_path in enumerate(paths):
            if image_path.endswith("_disp.jpg"):
                # don't try to predict disparity for a disparity image!
                continue

            # Load image and preprocess
            input_image = pil.open(image_path).convert('RGB')
            original_width, original_height = input_image.size
            input_image = input_image.resize((feed_width, feed_height), pil.LANCZOS)
            input_image = transforms.ToTensor()(input_image).unsqueeze(0)

            # PREDICTION
            start = time.time()

            input_image = input_image.to(device)
            features = encoder(input_image)
            outputs = depth_decoder(features)

            disp = outputs[("disp", 0)]
            disp_resized = torch.nn.functional.interpolate(
                disp, (original_height, original_width), mode="bilinear", align_corners=False)

            end = time.time()
            print("")
            print("time needed for prediction")
            print(end - start)
            print("")

            output_name = os.path.splitext(os.path.basename(image_path))[0]

            # Saving colormapped depth image
            disp_resized_np = disp_resized.squeeze().cpu().numpy()
            vmax = np.percentile(disp_resized_np, 95)
            normalizer = mpl.colors.Normalize(vmin=disp_resized_np.min(), vmax=vmax)
            mapper = cm.ScalarMappable(norm=normalizer, cmap='gray')
            colormapped_im = (mapper.to_rgba(disp_resized_np)[:, :, :3] * 255).astype(np.uint8)
            im = pil.fromarray(colormapped_im)

            name_dest_im = os.path.join(output_directory, "{}_disp.jpeg".format(output_name))
            im.save(name_dest_im)

            print("   Processed {:d} of {:d} images - saved prediction to {}".format(
                idx + 1, len(paths), name_dest_im))
            
    img = cv.imread('/home/pi/tirocinio/assets/eye_disp.jpeg', cv.IMREAD_GRAYSCALE)
    i=0
    for led in leds:
        #conversione da 0 a 100
        values[i]=int(round(100*img[led]/255))
        i=i+1
    print(values)

    apply_values_to_leds(pins, values)
    print('-> Done!')
    
camera.stop_preview()
camera.close()
p1.stop()
p2.stop()
p3.stop()
p4.stop()
p5.stop()
p6.stop()
p7.stop()
p8.stop()
GPIO.cleanup()
#calls another thread hearing for the restart of the program
t = threading.Thread(target=restart_program)
t.start()