from __future__ import absolute_import, division, print_function

from flask import Flask
from flask_restful import Resource, Api
from flask import jsonify, request

import os
import sys
import glob
import argparse
import numpy as np
import PIL.Image as pil
import matplotlib as mpl
import matplotlib.cm as cm

import torch
from torchvision import transforms, datasets

import networks
from layers import disp_to_depth
from utils import download_model_if_doesnt_exist

from time import sleep

import cv2 as cv
import time
from os import path

#one time initialization

if torch.cuda.is_available():
        device = torch.device("cuda")
else:
    device = torch.device("cpu")

# download model if not present
download_model_if_doesnt_exist("mono_640x192")
model_path = os.path.join("models", "mono_640x192")
print("-> Loading model from ", model_path)
encoder_path = os.path.join(model_path, "encoder.pth")
depth_decoder_path = os.path.join(model_path, "depth.pth")

# LOADING PRETRAINED MODEL
print("   Loading pretrained encoder")
encoder = networks.ResnetEncoder(18, False)
loaded_dict_enc = torch.load(encoder_path, map_location=device)

# extract the height and width of image that this model was trained with
feed_height = loaded_dict_enc['height']
feed_width = loaded_dict_enc['width']
filtered_dict_enc = {k: v for k, v in loaded_dict_enc.items() if k in encoder.state_dict()}
encoder.load_state_dict(filtered_dict_enc)
encoder.to(device)
encoder.eval()

print("   Loading pretrained decoder")
depth_decoder = networks.DepthDecoder(
    num_ch_enc=encoder.num_ch_enc, scales=range(4))

loaded_dict = torch.load(depth_decoder_path, map_location=device)
depth_decoder.load_state_dict(loaded_dict)

depth_decoder.to(device)
depth_decoder.eval()

#stop one time initialization
leds = list()
leds.append((0,0))
leds.append((0,158))
leds.append((0,316))
leds.append((0,637))
leds.append((180,0))
leds.append((189,158))
leds.append((189,316))
leds.append((189,637))

app = Flask(__name__)
api = Api(app)

eye = os.path.join("assets", "eye.jpg")
processed_eye = os.path.join("assets", "eye_disp.jpeg")
yolo = os.path.join("assets", "yolo.jpg")

############# YOLO INITIALIZATION ###########
yolo_folder = "yolo-coco"
# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.join(yolo_folder, "coco.names")
LABELS = open(labelsPath).read().strip().split("\n")

# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3), dtype="uint8")

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.join(yolo_folder, "yolov3.weights")
configPath = os.path.join(yolo_folder, "yolov3.cfg")

# load our YOLO object detector trained on COCO dataset (80 classes)
print("[INFO] loading YOLO from disk...")
net = cv.dnn.readNetFromDarknet(configPath, weightsPath)
# to enable is required compilation of opencv over GPU
#net.setPreferableBackend(cv.dnn.DNN_BACKEND_CUDA)
#net.setPreferableTarget(cv.dnn.DNN_TARGET_CUDA)

# determine only the *output* layer names that we need from YOLO
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

min_confidence = 0.5
threshold = 0.3


@app.route("/img", methods=["POST"])
def process_image():
    file = request.files['image']
    file.save(eye)
    return "OK"



class Vision(Resource):
    def get(self):
        values = [0]*8
        lables = []
        image_path = eye
        output_directory = os.path.dirname(image_path)

        with torch.no_grad():
            if path.exists(image_path):
                
                # Load image and preprocess
                input_image = pil.open(image_path).convert('RGB')
                original_width, original_height = input_image.size
                input_image = input_image.resize((feed_width, feed_height), pil.LANCZOS)
                input_image = transforms.ToTensor()(input_image).unsqueeze(0)

                # PREDICTION
                input_image = input_image.to(device)
                features = encoder(input_image)
                outputs = depth_decoder(features)

                disp = outputs[("disp", 0)]
                disp_resized = torch.nn.functional.interpolate(
                    disp, (original_height, original_width), mode="bilinear", align_corners=False)
                output_name = os.path.splitext(os.path.basename(image_path))[0]

                # Saving colormapped depth image
                disp_resized_np = disp_resized.squeeze().cpu().numpy()
                vmax = np.percentile(disp_resized_np, 95)
                normalizer = mpl.colors.Normalize(vmin=disp_resized_np.min(), vmax=vmax)
                mapper = cm.ScalarMappable(norm=normalizer, cmap='gray')
                colormapped_im = (mapper.to_rgba(disp_resized_np)[:, :, :3] * 255).astype(np.uint8)
                im = pil.fromarray(colormapped_im)

                name_dest_im = os.path.join(output_directory, "{}_disp.jpeg".format(output_name))
                im.save(name_dest_im)

        img = cv.imread(processed_eye, cv.IMREAD_GRAYSCALE)
        i=0
        for led in leds:
            #conversion to values from 0 to 100
            values[i]=int(round(100*img[led]/255))
            i=i+1
        print(values)
        #delete object, frees memory
        del img

        #object recognition
        boxes = []
        confidences = []
        classIDs = []

        # load our input image and grab its spatial dimensions
        im  = cv.imread(eye)
        #added borders to make the image square
        image = cv.copyMakeBorder(im, 224, 224, 0, 0, cv.BORDER_CONSTANT, value=[0,0,0,])
        (H, W) = image.shape[:2]

        # construct a blob from the input image and then perform a forward
        # pass of the YOLO object detector, giving us our bounding boxes and
        # associated probabilities
        blob = cv.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
            swapRB=True, crop=False)
        net.setInput(blob)
        start = time.time()
        layerOutputs = net.forward(ln)
        end = time.time()

        # show timing information on YOLO
        print("[INFO] YOLO took {:.6f} seconds".format(end - start))


        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:
                # extract the class ID and confidence (i.e., probability) of
                # the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > min_confidence:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # update our list of bounding box coordinates, confidences,
                    # and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)
        # apply non-maxima suppression to suppress weak, overlapping bounding boxes
        idxs = cv.dnn.NMSBoxes(boxes, confidences, min_confidence,
            threshold)

        # ensure at least one detection exists
        if len(idxs) > 0:
            # loop over the indexes we are keeping
            for i in idxs.flatten():
                # extract the bounding box coordinates
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])

                # draw a bounding box rectangle and label on the image
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv.rectangle(image, (x, y), (x + w, y + h), color, 2)
                
                #adds to labels list the classes names
                lables.append(LABELS[classIDs[i]])

                text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
                cv.putText(image, text, (x, y - 5), cv.FONT_HERSHEY_SIMPLEX,
                    0.5, color, 2)
        #saves the output image
        cv.imwrite(yolo, image)
        #returns lists with values and lables
        return jsonify(values, lables)

api.add_resource(Vision, '/')

if __name__ == '__main__':
    #starts server inside the current network
    app.run(host= '0.0.0.0')